package pl.bartek.dec;

import java.util.List;

public class Dog extends Animal {
    public Dog(String name, TypeOfAnimal type, List<Integer> allergies, List<KindOffFood> kindOffFoods,boolean isHungry) {
        super(name, type, allergies, kindOffFoods,isHungry);
    }
}
