package pl.bartek.dec;

import java.util.List;
import java.util.Random;

public abstract class Animal {
    private static Random random = new Random();
    private int age;
    private String name;
    private TypeOfAnimal type;
    private int numberOfLimb;
    private List<Integer> allergies;
    private List<KindOffFood> kindOffFoods;
    private TypeOfVoice typeOfVoice;
    private boolean isHungry;


    public Animal(String name, TypeOfAnimal type, List<Integer> allergies, List<KindOffFood> kindOffFoods, boolean isHungry) {
        this.name = name;
        this.type = type;
        this.allergies = allergies;
        this.kindOffFoods = kindOffFoods;
        this.isHungry = isHungry;
    }

    public List<KindOffFood> getKindOffFoods() {
        return kindOffFoods;
    }

    public TypeOfVoice getTypeOfVoice() {
        return typeOfVoice;
    }

    public void setTypeOfVoice(TypeOfVoice typeOfVoice) {
        this.typeOfVoice = typeOfVoice;
    }

    public List<KindOffFood> getKindOfFoods() {
        return kindOffFoods;
    }

    public void setKindOffFoods(List<KindOffFood> kindOffFoods) {
        this.kindOffFoods = kindOffFoods;
    }

    public List<Integer> getAllergies() {
        return allergies;
    }

    public void setAllergies(List<Integer> allergies) {
        this.allergies = allergies;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TypeOfAnimal getType() {
        return type;
    }

    public void setType(TypeOfAnimal type) {
        this.type = type;
    }

    public int getNumberOfLimb() {
        return numberOfLimb;
    }

    public void setNumberOfLimb(int numberOfLimb) {
        this.numberOfLimb = numberOfLimb;
    }

    public boolean isHungry() {
        return isHungry;
    }

    public void setHungry(boolean hungry) {
        isHungry = hungry;
    }

    public boolean feedMe(KindOffFood kindOfPersonFood, int indexOfAnimalFood, int missingAmountOfFood) {
        int amountEaten;
        int amountAvailable;

        if (missingAmountOfFood < 0) {//blok używany gdy zwierze zjada troche
            amountEaten = missingAmountOfFood + this.getKindOfFoods().get(indexOfAnimalFood).getAmountOfFood();
            amountAvailable = kindOfPersonFood.getAmountOfFood();
            kindOfPersonFood.setAmountOfFood(amountAvailable - amountEaten);

        } else if (missingAmountOfFood == 0) {//blok używany zwierz najada się za pierwszym razem
            amountEaten = missingAmountOfFood + this.getKindOfFoods().get(indexOfAnimalFood).getAmountOfFood();
            amountAvailable = kindOfPersonFood.getAmountOfFood();
            kindOfPersonFood.setAmountOfFood(amountAvailable - amountEaten);

        } else {//blok używany gdy zwierze dojada reszte
            amountEaten = missingAmountOfFood;
            amountAvailable = kindOfPersonFood.getAmountOfFood();
            kindOfPersonFood.setAmountOfFood(amountAvailable - amountEaten);
        }

        return false;
    }

    public String printFoods() {
        String string = "";
        for (int i = 0; i < kindOffFoods.size(); i++) {
            if (i == kindOffFoods.size() - 1) {
                string += kindOffFoods.get(i).getFood().toString() + ".";
            } else {
                string += kindOffFoods.get(i).getFood().toString() + ", ";
            }
        }

        return string;
    }

    public void makeSomeNoice() {
        System.out.println(this.getName() + ": " + this.getTypeOfVoice());
    }

    public void changeApetite(Animal animal, KindOffFood kindOffFood) {
        int newAmountOfFood;
        TypeOfAnimal type = animal.getType();

        switch (type) {
            case dog:
                newAmountOfFood = random.nextInt(5) + 1;
                kindOffFood.setAmountOfFood(newAmountOfFood);
                break;
            case cat:
                newAmountOfFood = random.nextInt(3) + 1;
                kindOffFood.setAmountOfFood(newAmountOfFood);
                break;
            case fish:
                newAmountOfFood = random.nextInt(2) + 1;
                kindOffFood.setAmountOfFood(newAmountOfFood);
                break;
        }
    }

    public  void changeHunger() {
        isHungry = true;
    }
}
