package pl.bartek.dec;

import java.util.List;

public class Cat extends Animal{
    public Cat(String name, TypeOfAnimal type, List<Integer> allergies, List<KindOffFood> kindOffFoods,boolean isHungry) {
        super(name, type, allergies, kindOffFoods,isHungry);
    }
}
