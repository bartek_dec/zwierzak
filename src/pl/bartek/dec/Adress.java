package pl.bartek.dec;

public class Adress {
    private String adres;

    public Adress(String adres) {
        this.adres = adres;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }
}
