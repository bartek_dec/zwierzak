package pl.bartek.dec;

public class KindOffFood implements Comparable<KindOffFood> {
    private Food food;
    private Integer amountOfFood;

    public KindOffFood() {

    }

    public KindOffFood(Food food, Integer amountOfFood) {
        this.food = food;
        this.amountOfFood = amountOfFood;
    }

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    public Integer getAmountOfFood() {
        return amountOfFood;
    }

    public void setAmountOfFood(Integer amountOfFood) {
        this.amountOfFood = amountOfFood;
    }

    @Override
    public int compareTo(KindOffFood o) {
        return this.getFood().compareTo(o.getFood());
    }


    @Override
    public boolean equals(Object obj) {

        if (obj instanceof KindOffFood) {
            boolean result = false;

            KindOffFood kindOffFood = (KindOffFood) obj;//rzutuje obj na KindOffFeed

            if (this.getFood() == kindOffFood.getFood()) {
                result = true;

            }
        }
        return true;
    }
}
