package pl.bartek.dec;

import java.util.List;

public class Bird extends Animal{
    public Bird(String name, TypeOfAnimal type, List<Integer> allergies, List<KindOffFood> kindOffFoods,boolean isHungry) {
        super(name, type, allergies, kindOffFoods,isHungry);
    }
}
