package pl.bartek.dec;

import java.util.ArrayList;
import java.util.List;


public class Main {
    public static void main(String[] args) {

/////////////// Tworze i wypełniam liste jedzenia dla pierwszego psa ///////////////
        List<KindOffFood> jedzeniePsa1 = new ArrayList<KindOffFood>();
        jedzeniePsa1.add(new KindOffFood(Food.bone, 1));
        jedzeniePsa1.add(new KindOffFood(Food.water, 1));

/////////////// Tworze i wypełniam liste jedzenia dla drugiego psa ///////////////
        List<KindOffFood> jedzenieKota = new ArrayList<KindOffFood>();
        jedzenieKota.add(new KindOffFood(Food.bone, 1));
        jedzenieKota.add(new KindOffFood(Food.milk, 1));

/////////////// Tworze i wypełniam liste jedzenia człowieka //////////////////////
        List<KindOffFood> jedzenieCzłowieka = new ArrayList<KindOffFood>();
        jedzenieCzłowieka.add(new KindOffFood(Food.bone, 1));
        jedzenieCzłowieka.add(new KindOffFood(Food.water, 1));

/////////////////////////////////////////////////////////////////////////////////
        List<Integer> alergiePsa = new ArrayList<Integer>();
        List<Integer> alergieKota = new ArrayList<Integer>();
        List<Integer> alergieOsoby = new ArrayList<Integer>();

        alergiePsa.add(1);
        alergiePsa.add(2);

        alergieKota.add(4);
        alergieKota.add(5);

        alergieOsoby.add(1);
        alergieOsoby.add(3);

        List<Animal> listaZwierzat = new ArrayList<Animal>();

//////////////////////////// Tworze psy ///////////////////////////////////////////
        Animal pies = new Dog("REKSIO", TypeOfAnimal.dog, alergiePsa, jedzeniePsa1, true);
        Animal kot = new Cat("FILEMON", TypeOfAnimal.cat, alergieKota, jedzenieKota, true);

        listaZwierzat.add(pies);
        listaZwierzat.add(kot);

//////////////////////////// Tworze człowieka ///////////////////////////////////////////
        Person czlowiek = new Person("Bartek", "Dec", 30, Sex.male, new Adress("Kamionka"),
                listaZwierzat, alergieOsoby, jedzenieCzłowieka);



        Thread personThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = 0; i < 25; i++) {
                        System.out.println();
                        czlowiek.feedAnimals();
                        Thread.sleep(1346);
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });


        Thread piesThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = 0; i < 25; i++) {
                        System.out.println();
                        pies.changeHunger();
                        Thread.sleep(1998);
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread kotThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = 0; i < 25; i++) {
                        System.out.println();
                        kot.changeHunger();
                        Thread.sleep(1798);
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        personThread.start();
        piesThread.start();
        kotThread.start();
    }
}
