package pl.bartek.dec;

import java.util.List;
//import java.util.Random;


public class Person {
    //private static Random random = new Random();
    public static final int TYPICAL_AMOUNT_TO_BUY = 10;
    private String name;
    private String surname;
    private int age;
    private Sex sex;
    private Adress adres;
    private List<Animal> myAnimals;
    private List<Integer> allergies;
    private List<KindOffFood> availableFood;

    public Person(String name, String surname, int age, Sex sex, Adress adres, List<Animal> myAnimals, List<Integer> allergies,
                  List<KindOffFood> availableFood) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.sex = sex;
        this.adres = adres;
        this.myAnimals = myAnimals;
        this.allergies = allergies;
        this.availableFood = availableFood;
    }

    public List<KindOffFood> getAvailableFood() {
        return availableFood;
    }

    public void setAvailableFood(List<KindOffFood> availableFood) {
        this.availableFood = availableFood;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public Adress getAdres() {
        return adres;
    }

    public void setAdres(Adress adres) {
        this.adres = adres;
    }

    public List<Animal> getMyAnimals() {
        return myAnimals;
    }

    public void setMyAnimals(List<Animal> myAnimals) {
        this.myAnimals = myAnimals;
    }

    public List<Integer> getAllergies() {
        return allergies;
    }

    public void setAllergies(List<Integer> allergies) {
        this.allergies = allergies;
    }

    public  void feedAnimals() {
        for (Animal animal : myAnimals) {  //iteruje po liście zwierzątek

            int sizeOfAnimalFoodList = animal.getKindOfFoods().size();
            KindOffFood kindOfAnimalFood;
            KindOffFood kindOfPersonFood;
            int indexOfPersonFood;
            int amountOfPersonFood;
            int amountOfMissingFood;
            boolean isHungry = animal.isHungry(); //sprawdzam czy zwierze jest głodne przed karmieniem

            if (isHungry) {

                for (int i = 0; i < sizeOfAnimalFoodList; i++) {  //dla każdego zwierzątka iteruje po liście jedzenia, które lubi

                    kindOfAnimalFood = animal.getKindOfFoods().get(i);
                    indexOfPersonFood = checkFoodInPersonList(kindOfAnimalFood);

                    if (indexOfPersonFood >= 0) {// jeśli osoba ma dany produkt na swojej liście to sprawdza czy wystarczy do nakarmienia

                        amountOfPersonFood = AmIAbleToFeed(indexOfPersonFood, i, animal); //sprawdzam czy wystarczy jedzenia aby nakarmić
                        kindOfPersonFood = this.getAvailableFood().get(indexOfPersonFood);

                        if (amountOfPersonFood >= 0) { //jeśli osoba ma wystarczająco jedzenia to karmi


                            animal.feedMe(kindOfPersonFood, i, 0);
                            displayOutputAfterFeeding(animal, kindOfAnimalFood.getAmountOfFood(), kindOfAnimalFood.getFood());
                            animal.changeApetite(animal, kindOfAnimalFood);

                        } else {//jeśli osoba nie ma wystarczająco jedzenia to karmi tym co ma

                            int amountOfFoodBeforeFeeding = kindOfPersonFood.getAmountOfFood();
                            amountOfMissingFood = amountOfFoodBeforeFeeding - animal.getKindOfFoods().get(i).getAmountOfFood();//liczba ujemna

                            if (amountOfFoodBeforeFeeding > 0) {//jeśli ma troche jedzenia

                                animal.feedMe(kindOfPersonFood, i, amountOfMissingFood); //karmie tym ile mam
                                displayOutputAfterPreFeeding(animal, amountOfFoodBeforeFeeding, kindOfAnimalFood.getFood());
                                doShoping(kindOfPersonFood, indexOfPersonFood, amountOfMissingFood); //dokupuje

                                int amountToBeEatenAgain = -1 * amountOfMissingFood;

                                animal.feedMe(kindOfPersonFood, i, amountToBeEatenAgain); //dokarmiam
                                displayOutputAfterAdditionalFeeding(animal, amountToBeEatenAgain, kindOfAnimalFood.getFood());
                                animal.changeApetite(animal, kindOfAnimalFood);

                            } else { //jeśli nie ma w ogóle
                                System.out.println("You don't have required food. Do shopping");
                                doShoping(kindOfPersonFood, indexOfPersonFood, amountOfMissingFood); //dokupuje

                                animal.feedMe(kindOfPersonFood, i, 0); //karmie
                                displayOutputAfterFeeding(animal, kindOfAnimalFood.getAmountOfFood(), kindOfAnimalFood.getFood());
                                animal.changeApetite(animal, kindOfAnimalFood);
                            }

                        }
                    } else { // jeśli osoba nie ma danego produktu na swojej liście to kupuje, dodaje do listy i karmi
                        KindOffFood kindOfFoodToBuy = new KindOffFood();
                        kindOfFoodToBuy.setFood(animal.getKindOfFoods().get(i).getFood());
                        kindOfFoodToBuy.setAmountOfFood(animal.getKindOfFoods().get(i).getAmountOfFood());

                        amountOfMissingFood = -1 * (animal.getKindOfFoods().get(i).getAmountOfFood());//liczba ujemna

                        doShoping(kindOfFoodToBuy, indexOfPersonFood, amountOfMissingFood);

                        KindOffFood kindOfFoodToFeed = getAvailableFood().get(availableFood.size() - 1);

                        animal.feedMe(kindOfFoodToFeed, i, 0);
                        displayOutputAfterFeeding(animal, kindOfFoodToBuy.getAmountOfFood(), kindOfFoodToBuy.getFood());
                        animal.changeApetite(animal, kindOfAnimalFood);
                    }
                }
                animal.setHungry(false);
            } else {
                System.out.println(animal.getName() + " is not hungry");
            }
        }
    }

    private int AmIAbleToFeed(int indexOfPersonFood, int indexOfAnimalFood, Animal animal) {

        int amountOfAvailablePersonFood = this.getAvailableFood().get(indexOfPersonFood).getAmountOfFood();
        int amountOfEatenFoodByAnimal = animal.getKindOfFoods().get(indexOfAnimalFood).getAmountOfFood();

        return amountOfAvailablePersonFood - amountOfEatenFoodByAnimal;
    }

    private int checkFoodInPersonList(KindOffFood kindOfAnimalFood) {  //sprawdzam czy osoba ma to jedzenie, które lubi zwierzątko

        int sizeOfPersonFoodList = this.getAvailableFood().size();
        Food animalFood = kindOfAnimalFood.getFood();
        Food personFood;
        int result = -1;

        for (int i = 0; i < sizeOfPersonFoodList; i++) {
            personFood = this.getAvailableFood().get(i).getFood();

            if (animalFood.compareTo(personFood) == 0) {  //jeśli osoba ma to jedzenie to zwracam jego indeks
                result = i;
                return result;
            }
        }
        return result;  //jeśli osoba nie ma tego jedzenia to zwracam -1
    }

    public boolean canHaveAnimal(Animal animal) {
        int size = animal.getAllergies().size();

        for (int i = 0; i < size; i++) {
            Integer animalAllergie = animal.getAllergies().get(i);
            Integer personAllergie = this.allergies.get(i);

            if (animalAllergie == personAllergie) {
                return false;
            }
        }
        return true;
    }

    public String printFood(int index) {
        return this.getAvailableFood().get(index).getFood().toString();
    }

    private void doShoping(KindOffFood kindOfFood, int indexInPersonList, int missingAmountOfFood) {
        KindOffFood kindOfPersonFood;
        int amountOfMissingAnimalFood = -1 * missingAmountOfFood;

        if (amountOfMissingAnimalFood >= TYPICAL_AMOUNT_TO_BUY) {//jeśli zwierzak musi dojeść więcej niż zazayczaj kupujemy

            if (indexInPersonList >= 0) { //jeśli osoba ma takie jedzenie na swojej liście to dokupuje tyle aby dokarmić + 10
                kindOfPersonFood = this.getAvailableFood().get(indexInPersonList);
                kindOfPersonFood.setAmountOfFood(TYPICAL_AMOUNT_TO_BUY + amountOfMissingAnimalFood);
            } else { //jeśli osoba nie ma takiego jedzenia na swojej liście to kupuje tyle aby dokarmić + 10
                KindOffFood newPersonKindOfFood = new KindOffFood(kindOfFood.getFood(),
                        TYPICAL_AMOUNT_TO_BUY + amountOfMissingAnimalFood);

                this.getAvailableFood().add(newPersonKindOfFood);
                setAvailableFood(this.getAvailableFood());
            }
        } else {//jeśli zwierzak musi dojeść mniej niż zazayczaj kupujemy
            if (indexInPersonList >= 0) { //jeśli osoba ma takie jedzenie na swojej liście to dokupuje 10 sztuk
                kindOfPersonFood = this.getAvailableFood().get(indexInPersonList);
                kindOfPersonFood.setAmountOfFood(TYPICAL_AMOUNT_TO_BUY);
            } else { //jeśli osoba nie ma takiego jedzenia na swojej liście to kupuje 10 sztuk
                KindOffFood newPersonKindOfFood = new KindOffFood(kindOfFood.getFood(),
                        TYPICAL_AMOUNT_TO_BUY);

                this.getAvailableFood().add(newPersonKindOfFood);
                setAvailableFood(this.getAvailableFood());
            }
        }


    }

    public void introduceYoursefl() {
        System.out.println("My name is " + this.getName() + " " + this.getSurname());
    }

    public void introduceMyAnimals() {
        for (Animal animal : myAnimals) {
            animal.makeSomeNoice();
        }
    }

    private void displayOutputAfterFeeding(Animal animal, Integer amount, Food food) {

        switch (food) {
            case bone:
            case carrot:
            case biscuit:
                if (amount == 1) {
                    System.out.println(animal.getName() + " ate " + amount + " " + food);
                } else {
                    System.out.println(animal.getName() + " ate " + amount + " " + food + "s");
                }
                break;
            case meat:
            case bread:
            case lettuce:
                if (amount == 1) {
                    System.out.println(animal.getName() + " ate " + amount + " piece of " + food);
                } else {
                    System.out.println(animal.getName() + " ate " + amount + " pieces of " + food);
                }
                break;
            case water:
            case milk:
                if (amount == 1) {
                    System.out.println(animal.getName() + " drunk " + amount + " portion of " + food);
                } else {
                    System.out.println(animal.getName() + " drunk " + amount + " portions of " + food);
                }
        }
    }

    private void displayOutputAfterPreFeeding(Animal animal, Integer amount, Food food) {

        switch (food) {
            case bone:
            case carrot:
            case biscuit:
                if (amount == 1) {
                    System.out.println(animal.getName() + " ate only " + amount + " " + food);
                } else {
                    System.out.println(animal.getName() + " ate only " + amount + " " + food + "s");
                }
                break;
            case meat:
            case bread:
            case lettuce:
                if (amount == 1) {
                    System.out.println(animal.getName() + " ate only " + amount + " piece of " + food);
                } else {
                    System.out.println(animal.getName() + " ate only " + amount + " pieces of " + food);
                }
                break;
            case water:
            case milk:
                if (amount == 1) {
                    System.out.println(animal.getName() + " drunk only " + amount + " portion of " + food);
                } else {
                    System.out.println(animal.getName() + " drunk only " + amount + " portions of " + food);
                }
        }
    }

    private void displayOutputAfterAdditionalFeeding(Animal animal, Integer amount, Food food) {

        switch (food) {
            case bone:
            case carrot:
            case biscuit:
                if (amount == 1) {
                    System.out.println(animal.getName() + " ate additional " + amount + " " + food);
                } else {
                    System.out.println(animal.getName() + " ate additional " + amount + " " + food + "s");
                }
                break;
            case meat:
            case bread:
            case lettuce:
                if (amount == 1) {
                    System.out.println(animal.getName() + " ate additional " + amount + " piece of " + food);
                } else {
                    System.out.println(animal.getName() + " ate additional " + amount + " pieces of " + food);
                }
                break;
            case water:
            case milk:
                if (amount == 1) {
                    System.out.println(animal.getName() + " drunk additional " + amount + " portion of " + food);
                } else {
                    System.out.println(animal.getName() + " drunk additional " + amount + " portions of " + food);
                }
        }
    }
}
